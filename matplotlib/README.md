Matplotlib
==========

[Full topic list](../README.md)

[Matplotlib](https://matplotlib.org/) is a plotting library for python that
you can use to have your scripts and notebooks automatically generate plots.
It can produce a very wide variety of plots and graphics in different styles
and can be interacted with in many different ways.

There is a page full of tutorials on the official site at
<https://matplotlib.org/users/tutorials.html>.

Installation
------------

If you're using the virtual machine provided as part of the TSM CDT this
  will already have been installed for you. Otherwise there are several
  different ways to do this. The easiest would be one of the following:

- matplotlib will be available through the package manager on the vast majority of
  Linux installations. E.g. on Ubuntu you could do `sudo apt install
  python3-matplotlib`.
- You can install it using the python package manager `pip`. E.g. to install
  the version for python3 system wide where you have sudo privileges you could
  do `sudo pip3 install matplotlib`.
- If you're using a
  [virtual environment](../python2/README.md#virtual-environments), and have
  it activated, you can install it with simply `pip install matplotlib`.

A First Example with Pyplot
---------------------------

The easiest way to get started with matplotlib is with the pyplot submodule.
This works in a similar way as plotting in Matlab. There's a full tutorial on
using this online at <https://matplotlib.org/users/pyplot_tutorial.html>.

An example showing how to use this to generate a basic plot of sin(x) is given
in `sinxplot.py`:

```python
#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np

# We set up a numpy array covering the range we want to plot.
xvals = np.linspace(-5, 5, 500)

# Now we use these x-values to generate a series of coordinates to plot with
# whatever function we like. In this case we're using the numpy sin function.
# The plot() function is for plotting lines and points in 2D.
plt.plot(xvals, np.sin(xvals))

# We can add axes labels and a plot title.
plt.xlabel('x')
plt.ylabel('sin(x)')
plt.title('Matplotlib Test Plot')

# And finally we display our plot.
plt.show()
```

If you run this script you'll see a window appear with your plot. You can
interact with it in various ways via the menu buttons, such as panning and
zooming the plot. There is also a save button which allows you to save the
image to various formats.

A More Complex Example
----------------------

You can combine several plots together and control the appearance as shown
in `multiplot.py`:

```python
#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np

# This time will make a figure with two subplots arranged vertically.
# First we initialize our figure
plt.figure()

# Again We set up numpy arrays covering the range we want to plot.
xvals1 = np.linspace(-5, 5, 500)
xvals2 = np.linspace(-5, 5, 20)

# This creates a subplot on a notional grid with 2 rows and 1 column and
# selects the 1st plot (top row).
plt.subplot(211)

# Plot both sin(x) with a blue solid line and cos(x) with red dashed line. We
# can do this with a single plot() command.
plt.plot(xvals1, np.sin(xvals1), 'b-', xvals1, np.cos(xvals1), 'r--')

# And add a legend
plt.legend(["sin", "cos"])

# Select the lower plot
plt.subplot(212)

# Plot sinh(x) and cosh(x) with yellow triangles and green circles connected
# by lines. We do this as four plots - using the denser x value list for the
# lines, and sparser for the points.
# We can use several plot commands to do this.
plt.plot(xvals1, np.sinh(xvals1), 'y-')
plt.plot(xvals2, np.sinh(xvals2), 'y^', label="sinh")
plt.plot(xvals1, np.cosh(xvals1), 'g-')
plt.plot(xvals2, np.cosh(xvals2), 'go', label="cosh")
# Let also fill the area between the curves, but make it semi-transparent
# using the fill_between() function.
plt.fill_between(xvals1, np.cosh(xvals1), np.sinh(xvals1), facecolor='green',
        alpha=0.2)

# Tune a few other settings
plt.grid(True)
plt.xlabel('x')
plt.ylabel('y')
# Since we've set some labels in the plot commands above we can call
# legend() without any arguments.
plt.legend()

# And finally we display our plot.
plt.show()
```

Further Reading
---------------

If you want to see what kind of plots can be generated, or if you have a
particular type of plot in mind that you want to generate, you can browse
through the gallery at <https://matplotlib.org/gallery.html>. Selecting any
image will show you the source code used to create it.
