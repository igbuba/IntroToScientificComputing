{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import scipy.linalg\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Matrix Diagonalization\n",
    "\n",
    "Matrix diagonalization is another type of matrix factorization. We already saw the LU factorization when we looked at how to solve linear systems of equations.\n",
    "\n",
    "Say we have a square matrix $A$, to diagonalize it we want to factorize it as\n",
    "$$ A = PDP^{-1} $$\n",
    "where $D$ is a diagonal matrix (only the diagonal elements are non-zero). Here the matrix $P$ is composed of the eigenvectors ($\\vec v_i$) of $A$, and the elements of $D$ are the eigenvalues ($\\lambda_i$) of $A$.\n",
    "\n",
    "So with the matrix $P$ composed of a set of column vectors $\\vec v_i$, we can also write the above expression as\n",
    "$$ A\\vec v_i = \\lambda_i\\vec v_i. $$\n",
    "\n",
    "You might recognize this form as that also taken by the time-independent Schrödinger equation: $\\mathrm{\\hat H}\\Psi = E\\Psi$. So matrix diagonalization arises very frequently in quantum mechanical models.\n",
    "\n",
    "We can rewrite Eq. (2) in a form like the linear systems we looked at previously\n",
    "$$ (A-\\lambda_i I)\\vec v_i = \\vec 0. $$\n",
    "We want to find solutions of this where $\\vec v$ is non-zero, which implies that the determinant of the matrix $(A-\\lambda I)$ must be zero."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Characteristic polynomial and eigenvalues\n",
    "\n",
    "This bring us to the characteristic polynomial of a matrix. This is defined by the expression above:\n",
    "$$\n",
    "\\textrm{det}(A-\\lambda I) = 0.\n",
    "$$\n",
    "\n",
    "For example, if we have the matrix\n",
    "$$ A = \\begin{pmatrix} 2 & 1 \\\\ 1 & 2 \\end{pmatrix}. $$\n",
    "This gives us the following expression for the characteristic polynomial:\n",
    "$$ \\textrm{det}\\begin{pmatrix} 2 - \\lambda & 1 \\\\ 1 & 2 - \\lambda \\end{pmatrix} = 0. $$\n",
    "We can expand this to\n",
    "$$ (2-\\lambda)^2 - 1 = 3 - 4 \\lambda + \\lambda^2 = 0. $$\n",
    "\n",
    "If we solve this equation for $\\lambda$ we'll obtain the eigenvalues of $A$. We can factor this as $(\\lambda-1)(\\lambda-3)$, meaning that our two eigenvalues are $\\lambda=1$ and $\\lambda=3$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Eigenvectors\n",
    "\n",
    "Once we have obtained the eigenvalues, we can obtain the eigenvectors corresponding to each one by solving the matrix equation. For example, to find the eigenvector corresponding to the eigenvalue $\\lambda = 1$ above, we need to solve\n",
    "$$ \\begin{pmatrix} 1 & 1 \\\\ 1 & 1 \\end{pmatrix} \\begin{pmatrix} v_1 \\\\ v_2 \\end{pmatrix} = \\begin{pmatrix} 0 \\\\ 0 \\end{pmatrix}. $$\n",
    "We can see that any $\\vec v$ where $v_1 = -v_2$ would satisfy this expression. Alternatively we could say the eigenvector is any (non-zero) scalar multiple of $\\begin{pmatrix} 1 \\\\ -1 \\end{pmatrix}$.\n",
    "\n",
    "For the eigenvalue $\\lambda=3$ we need to solve\n",
    "$$ \\begin{pmatrix} -1 & 1 \\\\ 1 & -1 \\end{pmatrix} \\begin{pmatrix} v_1 \\\\ v_2 \\end{pmatrix} = \\begin{pmatrix} 0 \\\\ 0 \\end{pmatrix}. $$\n",
    "In this case, any $\\vec v$ where $v_1 = v_2$ would satisfy this expression. So we can say the eigenvector is any scalar multiple of $\\begin{pmatrix} 1 \\\\ 1 \\end{pmatrix}$.\n",
    "\n",
    "> Note that since the eigenvector corresponding to a given eigenvalue can always be multiplied by a non-zero scalar and still be a valid eigenvector, they are often normalized such that $\\left|\\vec v\\right| = 1$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Numerical approaches for calculating eigenvalues\n",
    "\n",
    "Finding the eigenvalues from the characteristic polynomial in the example above was straightforward as this was just a $2\\times2$ matrix. The more general problem of finding the eigenvalues of an $n\\times n$ matrix is much more difficult, and is typically solved numerically for any moderately sized matrix. Many methods are available, each with their own advantages and disadvantages, in terms of scaling, stability, suitability for different types of matrices (e.g. sparse, banded, symmetric, real/complex, etc), and what information we want to find."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Power Iteration\n",
    "\n",
    "Power iteration can be used when we want to find the eigenvalue with largest absolute value in a matrix, and its associated eigenvector. These are termed the dominant eigenvalue and dominant eigenvector. This is one of the easier algorithms to follow. It consists of an iterative procedure to find the dominant eigenvalue and eigenvector of a matrix $A$:\n",
    "- Pick an initial guess for the eigenvector $\\vec v_0$. It must have a non-zero component along the dominant eigenvector.\n",
    "- Calculate $v_1=A\\vec v_0$.\n",
    "- Renormalize $\\vec v_1$.\n",
    "- $\\vec v_1$ is now a better approximation to the dominant eigenvector.\n",
    "- Repeat this process, with $\\vec v_{i+1} = A\\vec v_i/|A\\vec v_i|$ for each iteration, until a specified number of iterations have been completed or some other convergence criteria has been fulfilled.\n",
    "- The eigenvalue associated with an eigenvector can be calculated using the Rayleigh quotient:\n",
    "$$ \\lambda_i = \\frac{A \\vec v \\cdot \\vec v}{\\vec v \\cdot \\vec v}. $$\n",
    "    - If $\\vec v$ is normalized this is simplified to $\\lambda = A\\vec v \\cdot \\vec v$.\n",
    "\n",
    "Let's write a Python function to implement this algorithm:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def power_iteration(a, eig_conv=0.0001, max_iter=100):\n",
    "    '''\n",
    "    Find the dominant eigevalue and eigenvector of a square matrix.\n",
    "    \n",
    "    Use the power iteration algorithm to find the dominant\n",
    "    eigenvalue and eigenvector of a matrix. The eigenvalue\n",
    "    is evaluted on each step using the Rayleigh quotient and\n",
    "    convergence is tested against its relative change.\n",
    "    \n",
    "    Parameters\n",
    "    ----------\n",
    "    a : (N, N) array_like\n",
    "        The matrix we want to analyse, in numpy format.\n",
    "    eig_conv : (float, optional)\n",
    "        The process is converged when the relative change in the eigenvalue\n",
    "        is less than this number.\n",
    "    max_iter : (int, optional)\n",
    "        The maximum number of iterations before the function exits.\n",
    "        \n",
    "    Returns\n",
    "    -------\n",
    "    steps : int\n",
    "        The number of steps to convergence. This would not be returned in a\n",
    "        proper implementation, but it's interesting to see here how many steps\n",
    "        it actually takes to converge.\n",
    "    eigval : float\n",
    "        The dominant eigenvalue.\n",
    "    eigvec : array\n",
    "        The dominant eigenvector.\n",
    "        \n",
    "    Raises\n",
    "    ------\n",
    "    LinAlgError\n",
    "        If the max number of iterations are reached before convergence.\n",
    "                  \n",
    "    '''\n",
    "    # We can start from a random guess for the eigenvector.\n",
    "    eigvec = np.random.rand(a.shape[0])\n",
    "    eigvec = eigvec / np.linalg.norm(eigvec)\n",
    "    eigval = np.dot(np.dot(a, eigvec), eigvec)\n",
    "    \n",
    "    for steps in range(max_iter):\n",
    "        # Calculate our new eigvec from our previous approximation.\n",
    "        eigvec = np.dot(a, eigvec)\n",
    "        # Renormalize it.\n",
    "        eigvec = eigvec / np.linalg.norm(eigvec)\n",
    "        \n",
    "        # Calculate the new eigenvalue.\n",
    "        eigval_new = np.dot(np.dot(a, eigvec), eigvec)\n",
    "        # Test the relative change with respect to the previous approximation.\n",
    "        if abs((eigval_new - eigval) / eigval) < eig_conv:\n",
    "            return steps, eigval, eigvec\n",
    "        # Update our approximation for the eigenvalue.\n",
    "        eigval = eigval_new\n",
    "        \n",
    "    # If we finish the for loop without converging, raise an error.\n",
    "    raise np.linalg.LinAlgError('power_iteration failed to converge to requested tolerance.')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "power_iteration(np.array([[2, 1], [1, 2]]), eig_conv=1e-10)\n",
    "# For our simple system we had previously, you can converge very accurately in a\n",
    "# fairly small number of steps. Since we have a random starting point, the number\n",
    "# of steps to convergence can vary a bit."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Lets see what we get for a large random matrix\n",
    "rand_mat = np.random.rand(100, 100)\n",
    "power_iteration(rand_mat, eig_conv=1e-10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Try playing around with this for random matrices of different sizes. Do you notice anything about the leading eigenvalue each time?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In practice, convergence is most slow when there is a small separation between the dominant eigenvalue and the next most dominant. This means convergence will also be slow if the dominant eigenvalue is degenerate, in which case you'll converge to the eigenvector closest your initial guess, and this may not be consistent if you start from a random guess as above.\n",
    "\n",
    "This can be quite an efficient method for very large sparse matrices, as no explicit decomposition is needed. This is used, for example, by search engines to calculate the page rank of a website, or to show recommended users on social websites."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Let's see what happens for a matrix with a degenerate dominant eigenvalue\n",
    "m = np.array([[2, 1, 3], [0, 2, 3], [0, 0, 1]])\n",
    "np.linalg.eig(m)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "power_iteration(m, eig_conv=1e-5, max_iter=1000)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## QR Decomposition\n",
    "\n",
    "The approach most commonly used in many numerical libraries is based on QR decomposition. The QR decomposition, factorizes a square matrix $A$ as\n",
    "$$ A = QR, $$\n",
    "where $Q$ is an orthogonal matrix, meaning it's columns are orthogonal unit vectors, and $R$ is an upper triangular matrix (as was $U$ in the LU decomposition we covered previously).\n",
    "\n",
    "The QR algorithm proceeds as follows:\n",
    "- Starting from the matrix we wish to diagonalize, $A_0$, we calculate the QR decomposition, to find $Q_0$ and $R_0$.\n",
    "- We then calculate $A_1$ as $R_0 Q_0$.\n",
    "- The QR decomposition of $A_1$ is then found.\n",
    "- This process is iterated with $A_{i+1} = R_i Q_i$\n",
    "- $A_i$ will converge to an upper triangular matrix for most matrices. The diagonal elements of an upper triangular matrix equal its eigenvalues.\n",
    "- This works as $A_{i+1} = R_i Q_i = Q_i^{-1}Q_i R_i Q_i = Q_i^{-1}A_i Q_i$, meaning that $A_i$ and $A_{i+1}$ are similar (have the same eigenvalues) since $Q$ is an orthogonal matrix.\n",
    "- This method functions in a similar way to the power iteration method, but calculates all eigenvalues and eigenvectors simultaneously, by multiplying by a full matrix rather than a single vector.\n",
    "- In practice, some additional steps are done to optimize this process, such as using so-called [Householder transformations](https://en.wikipedia.org/wiki/Householder_transformation) to convert $A$ to an upper Hessenberg form (almost upper triangular), so implementations can be somewhat involved.\n",
    "- Also if you want to obtain the eigenvectors as well as eigenvalues, the product of the $Q_i$ of each step needs to be accumulated, effectively giving you what is known as the _Schur decomposition_ ($A=QUQ^{-1}$, where $Q$ is unitary and $U$ is upper triangular), which can then be used to find the eigenvectors efficiently."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Divide and Conquer\n",
    "\n",
    "We'll just go through a general overview of this to show you how different it is to the QR approach we discussed earlier. It's interesting how for certain types of matrices a very different approach can be more optimal then for a general matrix. In this case, the approach works out to be slightly more optimal for symmetric and hermitian matrices. It proceeds as follows:\n",
    "- The matrix $M$, is first converted to an tridiagonal form $T$, using [Householder transformations](https://en.wikipedia.org/wiki/Householder_transformation).\n",
    "- The tridiagonal matrix is then converted to a a block diagonal matrix plus a rank 1 correction matrix. This is the divide step, and produces the matrices from the upper and lower blocks $T_1$ and $T_2$, and the correction matrix $\\beta$.\n",
    "- Then there is conquer step that involves generating the diagonalization of the original matrix from the diagonalization of these submatrices.\n",
    "\n",
    "There's more detail on this algorithm at the [wiki page](https://en.wikipedia.org/wiki/Divide-and-conquer_eigenvalue_algorithm)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Diagonalizing Matrices in Python\n",
    "\n",
    "There are a range of functions available, both from Numpy and SciPy to find the eigenvalues and eigenvectors of matrices. These are all interfaces to various LAPACK (a fast Fortran linear algebra library) functions, with different functions offering more optimal approaches for certain matrix types (recall banded matrices had a better scaling approach when solving linear systems). Functions to return selveral of the factorizations we've discussed are also available.\n",
    "\n",
    "One thing to keep in mind is that the eigenvalues and eigenvectors of a real matrix can be complex, so these will usually return arrays with complex values even if your input is real, except in the case of a real symmetric or Hermitian matrix."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Numpy functions\n",
    "\n",
    "- [numpy.linalg.eigvals](https://docs.scipy.org/doc/numpy/reference/generated/numpy.linalg.eigvals.html) will compute the eigenvalues of a square array\n",
    "- [numpy.linalg.eig](https://docs.scipy.org/doc/numpy/reference/generated/numpy.linalg.eig.html) will compute the eigenvalues and eigenvectors of a square array.\n",
    "    - Both `eig()` and `eigvals()` use calls to the [geev](https://software.intel.com/en-us/mkl-developer-reference-fortran-geev) LAPACK routines for general square matrices, which uses the QR algorithm.\n",
    "    - There are also versions of these functions for Hermitian or real symmetric matrices: [numpy.linalg.eigvalsh](https://docs.scipy.org/doc/numpy/reference/generated/numpy.linalg.eigvalsh.html) and [numpy.linalg.eigh](https://docs.scipy.org/doc/numpy/reference/generated/numpy.linalg.eigh.html), which use the [syevd](https://software.intel.com/en-us/mkl-developer-reference-fortran-syevd) (real symmetric), and [heevd](https://software.intel.com/en-us/mkl-developer-reference-fortran-heevd) (hermitian) LAPACK routines. Both of these use a [divide and conquer algorithm](https://en.wikipedia.org/wiki/Divide-and-conquer_eigenvalue_algorithm).\n",
    "- [numpy.linalg.qr](https://docs.scipy.org/doc/numpy/reference/generated/numpy.linalg.qr.html) is also available for calculating the QR decomposition directly. This is also an interface to LAPACK routines."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## SciPy functions\n",
    "\n",
    "There are many similarly available SciPy functions, which are also interfaces to the same LAPACK libraries used by their NumPy equivalents, but offer many additional parameters to tune how the calculation is done, and what is returned.\n",
    "\n",
    "- [scipy.linalg.eigvals](https://docs.scipy.org/doc/scipy/reference/generated/scipy.linalg.eigvals.html) calculates eigenvalues, and [scipy.linalg.eig](https://docs.scipy.org/doc/scipy/reference/generated/scipy.linalg.eig.html) calculates eigenvalues and eigenvectors.\n",
    "- [scipy.linalg.eigvalsh](https://docs.scipy.org/doc/scipy/reference/generated/scipy.linalg.eigvalsh.html) and [scipy.linalg.eigh](https://docs.scipy.org/doc/scipy/reference/generated/scipy.linalg.eigh.html) do the same thing for real symmetric or Hermitian matrices.\n",
    "- In addition recent SciPy versions have dedicated functions for:\n",
    "    - Real symmetric or Hermitian banded matrices  [scipy.linalg.eigvals_banded](https://docs.scipy.org/doc/scipy/reference/generated/scipy.linalg.eigvals_banded.html) and [scipy.linalg.eig.banded](https://docs.scipy.org/doc/scipy/reference/generated/scipy.linalg.eig_banded.html)\n",
    "    - Real symmetric tridiagonal matrices [scipy.linalg.eigh_tridiagonal](https://docs.scipy.org/doc/scipy/reference/generated/scipy.linalg.eigh_tridiagonal.html) and [scipy.linalg.eigh_tridiagonal](https://docs.scipy.org/doc/scipy/reference/generated/scipy.linalg.eigh_tridiagonal.html) \n",
    "- There is also QR decomposition with [scipy.linalg.qr](https://docs.scipy.org/doc/scipy/reference/generated/scipy.linalg.qr.html) and many related functions that offer more advanced options relative to the NumPy version, such as the ability to also generate a pivot matrix which orders the diagonal elements of R.\n",
    "\n",
    "While neither NumPy or SciPy have a function specifically for a leading eigenvalue/vector as obtained from power iteration, the SciPy eigvalsh function can take an `eigvals` argument to restrict the calculation to only calculate certain eigenvalues. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "np.linalg.eigvals(rand_mat)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "scipy.linalg.eigvals(rand_mat)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "diag_timing_np = []\n",
    "diag_timing_sp = []\n",
    "size = np.arange(10, 2000, 400)\n",
    "for s in size:\n",
    "    print(\"Size = %dx%d, NumPy, SciPy timings:\" % (s, s))\n",
    "    m_rand = np.random.rand(s, s)\n",
    "    \n",
    "    timer_out = %timeit -r 3 -o np.linalg.eigvals(m_rand)\n",
    "    diag_timing_np.append(timer_out.average)\n",
    "    \n",
    "    timer_out = %timeit -r 3 -o scipy.linalg.eigvals(m_rand)\n",
    "    diag_timing_sp.append(timer_out.average)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot our timing results\n",
    "plt.plot(size, np.array(diag_timing_np), \"bo\", label=\"NumPy\")\n",
    "plt.plot(size, np.array(diag_timing_sp), \"r+\", label=\"SciPy\")\n",
    "scale = diag_timing_np[-1] / size[-1]**3\n",
    "plt.plot(size, scale * size**3, 'g-', label=\"N^3\")\n",
    "plt.xlabel(\"Matrix Size\")\n",
    "plt.ylabel(\"Time to find eigenvalues (s)\")\n",
    "plt.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Sparse Linear Algebra\n",
    "\n",
    "As you might imagine, certain algorithms are more optimal for certain types of system. Sparse matrices (where most of the elements are zero) are special enough, and occur commonly enough, that there is an entire SciPy module for space linear algebra: [scipy.linalg.sparse](https://docs.scipy.org/doc/scipy/reference/sparse.linalg.html) separate from the main linear algebra module.\n",
    "\n",
    "Many models can result in sparse matrices: for example finite element method solutions of partial differential equations.\n",
    "\n",
    "The module has many linear system solvers for problems such as we saw previously, and functions to find a particular number of eigenvalues of the matrix: [scipy.sparse.linalg.eigs](https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.linalg.eigs.html), or all eigenvalues/vectors: [scipy.sparse.linalg.lobpcg](https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.linalg.lobpcg.html).\n",
    "\n",
    "If you have represented your problem with a sparse matrix, these functions will be significantly faster. Also, while you can pass arrays in the usual array format, this can be wasteful as most elements are zero. For this reason, there are different ways to represent your matrix that are much more compact. These are given as classes in [scipy.sparse](https://docs.scipy.org/doc/scipy/reference/sparse.html)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## A Worked Example: Vibrations in a Methane Molecule\n",
    "\n",
    "The file [`CH4-dynmat`](CH4-dynmat) contains output from a calculation of the dynamical matrix of a methane molecule using [Density Functional Perutbation Theory](https://journals.aps.org/rmp/abstract/10.1103/RevModPhys.73.515) (which you will likely learn about in a later course). This is a quantum mechanical calculation of the derivative of the energy of a system with respect to displacements of pairs of atoms in each of the Cartesian directions.\n",
    "\n",
    "The eigenvalues of this matrix give the vibrational frequencies squared. Let's try to parse this file to a numpy array and diagonalize it to find the predicted vibrational frequencies of methane.\n",
    "\n",
    "The layout of the file is not amenable to being imported directly. You'll often find that if you want to process the output from some code one of the trickiest things is to get it in a format you can operate on. There are many recent initiatives such as [NetCDF](https://www.unidata.ucar.edu/software/netcdf/) and [HDF5](https://support.hdfgroup.org/HDF5/doc/index.html) which try to solve this, and are now incorporated into many scientific codes, but it's good to be able to work without them.\n",
    "\n",
    "So our problem can be broken down into three steps:\n",
    "1. Parse the data in the file to a NumPy array.\n",
    "2. Diagonalize the array to find the eigenvalues.\n",
    "3. Use the eigenvalues to calculate the vibration frequencies, and convert from whatever units they are in, to some more usual units for frequency, such as THz.\n",
    "\n",
    "If you were writing a code to do this, this could be a good way to break the problem down into functions.\n",
    "\n",
    "The actual eigenvalue expression we're solving can be written as:\n",
    "\n",
    "$$\n",
    "\\omega^2 e_{i,\\alpha} = \\Sigma_{j,\\beta}\\mathbf D_{i, j, \\alpha,\\beta} e_{i, \\alpha}\n",
    "$$\n",
    "\n",
    "where $i$ and $j$ index atoms, $\\alpha$ and $\\beta$ index directions, $\\omega$ is the vibration frequency, $e$ is the eigenvector of the vibration and $\\mathbf{D}$ is the dynamical matrix given by\n",
    "\n",
    "$$\n",
    "\\mathbf D_{i, j, \\alpha,\\beta} = \\frac{1}{\\sqrt{m_i m_j}} \\frac{\\partial^2 E}{\\partial x_{i,\\alpha}\\partial x_{j,\\beta}}\n",
    "$$\n",
    "where $m_i$ is the mass of atom $i$.\n",
    "\n",
    "Let's go through this step by step."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Parse the file \n",
    "\n",
    "It can often be easier to use a bash script or awk, or even a text editor if the file is not too big (and you're sure you only need to do it once), to manipulate the file into a more usable format.\n",
    "\n",
    "The file we're trying to analyse has the following format.\n",
    "- The first 15 lines are header information, with details of the atomic positions, masses, and wavevector of the calculation (the wavector only applies to periodic systems, and should always be $\\Gamma=(0,0,0)$ for a molecule). We will likely need the masses that are given in lines 4 and 5. And the atom indexes will help interpret the eigenvectors if we desire at the end.\n",
    "- Then we have the indices of the two atoms involved in the perturbation.\n",
    "- For each pair of atoms we have a 3x3 array (of complex numbers with the imaginary part=0) that give the actual dynamical matrix values without the mass factors above. These are in units of Ry/Bohr^2."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# np.loadtxt won't work for this even if we skip the header as we don't\n",
    "# have the same number of values in each row.\n",
    "#dynmat = np.loadtxt(\"CH4-dynmat\", skiprows=15)\n",
    "\n",
    "# It may be possible to figure out the right set of options to pass to\n",
    "# pandas.read_csv to get our data - something like \n",
    "# dynmat = pandas.read_csv(\"CH4-dynmat\", skiprows=15, skipinitialspace=True, sep=\" \")\n",
    "# might be a starting point.\n",
    "\n",
    "# Let's do it explicitly in python. If a code always generates output in the same format\n",
    "# this can be the most understandable thing to do.\n",
    "\n",
    "# Let's try to write a fairly general parser for output in this form.\n",
    "def parse_dynmat(filename):\n",
    "    '''Parse useful info from a dynmat.'''\n",
    "    \n",
    "    # First get a list of the lines in the file\n",
    "    f = open(filename, 'r')\n",
    "    file_lines = f.readlines()\n",
    "    f.close()\n",
    "\n",
    "    # Line 2 has the number of species and atoms\n",
    "    sys_info = file_lines[2].split()\n",
    "    nspecies = int(sys_info[0])\n",
    "    natoms = int(sys_info[1])\n",
    "\n",
    "    # Lines 3 to 2+nspecies have the index of each species\n",
    "    # and their masses. We'll save this in a list of dicts.\n",
    "    atom_info = []\n",
    "    for line in file_lines[3:3+nspecies]:\n",
    "        species_mass = line.split()\n",
    "        atom_info.append({'species': species_mass[1].lstrip(\"'\"),\n",
    "                           'mass': float(species_mass[3])})\n",
    "\n",
    "    # Lines 3+nspecies to 3+nspecies+natoms have the positions\n",
    "    # We could save everything in the same way, but we just need\n",
    "    # the mass of each atom index.\n",
    "    atom_mass = []\n",
    "    for line in file_lines[3+nspecies:3+nspecies+natoms]:\n",
    "        atom_type = int(line.split()[1]) - 1\n",
    "        atom_mass.append(atom_info[atom_type]['mass'])\n",
    "\n",
    "    # The dynamical matrix starts at 8+nspecies+natoms\n",
    "    counter = 0\n",
    "    dynmat = np.empty((3*natoms, 3*natoms))\n",
    "    for line in file_lines[8+nspecies+natoms:]:\n",
    "        if counter == 0:\n",
    "            # Save the atom indexes. We'll use these to generate indices\n",
    "            # for the dynamical matrix.\n",
    "            atom1, atom2 = [int(i) for i in line.split()]\n",
    "            massfactor = 1/np.sqrt(atom_mass[atom1-1]\n",
    "                                   * atom_mass[atom2-1])\n",
    "        else:\n",
    "            # Save the values such that they're in order\n",
    "            # of each atom+direction of perturbation.\n",
    "            index1 = 3*(atom1-1) + counter-1\n",
    "            index2 = 3*(atom2-1)\n",
    "            # We only take the real values, and multiply in the\n",
    "            # mass factor.\n",
    "            dynmat[index1, index2:index2+3] = np.array([float(val)\n",
    "                    for val in line.split()])[::2] * massfactor\n",
    "        counter += 1\n",
    "        if counter == 4:\n",
    "            counter -= 4\n",
    "    return(atom_info, dynmat)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Use this on our file and print the output to check it worked.\n",
    "atom_info, dynmat = parse_dynmat(\"CH4-dynmat\")\n",
    "print(atom_info, dynmat)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we have this in the right format, calculating the eigenvalues is relatively straightforward.\n",
    "\n",
    "Our dynamical matrix is (or at least it should be) symmetric, so we can use the Hermitian flavour of the function.\n",
    "For a well behaved function\n",
    "\n",
    "$$\n",
    "\\frac{\\partial^2 E}{\\partial x_{i,\\alpha}\\partial x_{j,\\beta}} = \\frac{\\partial^2 E}{\\partial x_{j,\\beta}\\partial x_{i,\\alpha}}\n",
    "$$\n",
    "for atoms $i$ and $j$, and directions $\\alpha$ and $\\beta$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "eigvals = scipy.linalg.eigvalsh(dynmat)\n",
    "print(eigvals)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we want to use these eigenvalues to calculate the vibrational frequencies. This involves taking the square root of the eigenvalues and changing the units to something understandable. We'll go for THz, which is usually good for molecular and crystal vibrational frequencies."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Convert energy, distance and mass to SI\n",
    "Ry_to_J = 2.1798741e-18\n",
    "Bohr_to_m = 5.29177e-11\n",
    "mass_to_kg = 2*9.10938356E-31 # Ry mass is electron mass*2\n",
    "\n",
    "omega_Hz = np.sqrt((Ry_to_J / Bohr_to_m ** 2) / mass_to_kg)\n",
    "# And convert angular frequency to freqe\n",
    "f_to_THz = omega_Hz * 1e-12 / (2 * np.pi)\n",
    "\n",
    "# Add 0j to make a complex before taking the square root, so it won't fail on negative values.\n",
    "np.sqrt(eigvals+0j) * f_to_THz"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We have a bit of a problem as not all our eigenvalues are positive, so I've made the value complex before taking the square root. \n",
    "\n",
    "Side Note1 - the usual convention when dealing with negative eigenvalues, is to quote the value instead as a negative frequency rather than an imaginary frequency. This is usually indicates an instability.\n",
    "\n",
    "Side Note2 - a molecule will have three modes corresponding to rotation and three to translation, which should have zero frequency. These are the three unstable, and three relatively low frequency modes we found here.\n",
    "\n",
    "In the end, thanks to NumPy and SciPy, finding the eigenvalues was the easiest part of this problem."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
