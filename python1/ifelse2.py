#!/usr/bin/env python3

a = int(input("Enter an integer: "))

if a > 10 and a%2 == 0:
    print(a, "is greater than 10 and even.")
elif a < 0 or a%3 == 0:
    print(a, "is negative or divisible by 3.")
else:
    print(a, "is none of the above.")
