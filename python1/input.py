#!/usr/bin/env python3

prompt = "Please enter some text: "
instring = input(prompt)
# This is the same as doing
# instring = input("Please enter some text: ")
print("The text you entered was:", instring)
