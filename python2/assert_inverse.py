#!/usr/bin/env python3

import sys

def inverse(num):
    """Return the inverse of a number."""
    # Use the "sys" module to find what the smallest float is. You could
    # also just use the smallest number you think is realistic for whatever
    # you're trying to do.
    assert abs(num) > sys.float_info.min, \
            "inverse() called with non-zero argument"
    return 1.0 / num

test_nums = 1, 1.1, 0.4, 0.1, 0.0
for num in test_nums:
    print("The inverse of", num, "is", inverse(num))
